/*
 * main.c
 *
 * Created: 25.12.2015 22:31:18
 *  Author: al
 */ 

#define F_CPU 12000000UL
#include <avr/interrupt.h>
#include <util/delay.h>
#include "spi.h"
#include "multiplex.h"
#include "UART.h"

extern const uint16_t chars[];

int main(void)
{
	setupSpi(2,0);
	UART_Init(MYUBRR);	// init serial with 9600 BAUD
	seg_setup();
	sei();
	
	uint8_t inByte;
  	
    while(1)
    {
		if (UART_getByte(&inByte))
		{
			if ( (inByte>31) && (inByte<=127) ) // if in char list
				shiftcharInRight(chars[inByte-32]);
			else
				shiftcharInRight(0);
		}
		//_delay_ms(10);	
    }
}