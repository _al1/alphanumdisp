/*
 * _7_seg.h
 *
 * Created: 25.06.2015 20:50:19
 *  Author: al1
 */ 


#ifndef SEG_H_
#define SEG_H_

	#include "7seg_config.h"
	#include <avr/io.h>
	
	void seg_setup (void);
	
	void shiftcharInLeft(uint16_t charIn);
	void shiftcharInRight(uint16_t charIn);
	
#endif /* SEG_H_ */