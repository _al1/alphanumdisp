/*
 * _7_seg.c
 *
 * Created: 25.06.2015 20:50:42
 *  Author: al1
 */ 

// digit 0 is the most left one!


#include <avr/io.h>
#include <avr/interrupt.h>
#include "gpio.h"
#include "spi.h"
#include "multiplex.h"
#include "chars.h"

#define seg_digits 6
	
volatile uint16_t seg_digitArray[seg_digits]={0x1589, 0x11a, 0x100a, 0x100a, 0x18b, 0x1459};
volatile uint8_t seg_pointer;


ISR(TIMER0_OVF_vect)
{	
	uint16_t outVAR = (seg_digitArray[seg_pointer]); // get digit which will be shown

 	SHIFT_OUTPUT_DISABlE;
 	putByteSpi(outVAR>>8);
 	putByteSpi(outVAR);
	
	PORTD |= (0x7E);				//clear all digits
	PORTD &= ~(1<<(seg_pointer+1));	//set digit	
	SET_SS;
	RESET_SS;
 	SHIFT_OUTPUT_ENABLE;
	
	seg_pointer++;
	if (seg_pointer>seg_digits)
		seg_pointer=0;		
}

void seg_setup (void)
{
	DDRB |= (1<<PORTB1);		// OE pin output 
	DDRD |= 0b01111110;	//enable all digitd
	PORTD |= 0b01111110;	// all high -> disable 
	RESET_SS;
	TCCR0 |= (1<<CS00)|(1<<CS01);	//prescaler 64
	TIMSK |= (1<<TOIE0);	// enable overflow interrupt

	seg_pointer=0;			//start with first digit
	
}

void shiftcharInLeft(uint16_t charIn)
{
	for (uint8_t i=seg_digits-1; (i>0); i--)
	{
		seg_digitArray[i]=seg_digitArray[i-1];
	}
	seg_digitArray[0]=charIn;
}

void shiftcharInRight(uint16_t charIn)
{
	for (uint8_t i=0; (i<seg_digits-1); i++)
	{
		seg_digitArray[i]=seg_digitArray[i+1];
	}
	seg_digitArray[seg_digits-1]=charIn;
}