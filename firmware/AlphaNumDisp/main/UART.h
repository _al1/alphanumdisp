/*
 * UART.h
 *
 * Created: 13.07.2015 18:41:03
 * Author: Marc/Alex
 * Provides UART for several ATmega �C. 
 */ 


#ifndef UART_H_
#define UART_H_

	#if defined (__AVR_ATmega328P__)
		#define RX_VECT USART_RX_vect		//RX interrupt vector
		#define TX_VECT USART_UDRE_vect		//TX interrupt vector
		#define TXRX_REG UDR0				//TX/RX register
		#define TXRX_REG_EMPTY_FLAG UDRE0	//TX/RX is empty flag
		#define CONTROL_REG_A UCSR0A		//UART control register A
		#define CONTROL_REG_B UCSR0B		//UART control register B
		#define CONTROL_REG_C UCSR0C		//UART control register C
		#define TX_INT_EN UDRIE0			//Enable TX interrupt flag
		#define RX_INT_EN RXCIE0			//enable RX interrupt flag
		#define TX_EN TXEN0					//Enable transmitter
		#define RX_EN RXEN0					//Enable receiver
		#define SEL_SIZE0 UCSZ00			//Select packet size bit0
		#define SEL_SIZE1 UCSZ01			//Select packet size bit1
		#define ASYNC_MODE U2X0				//UART mode sync=0, async=1
		#define BAUD_REGH UBRR0H			//16bit baud rate register high byte
		#define BAUD_REGL UBRR0L			//16bit baud rate register low byte

	#elif defined (__AVR_ATmega2560__)
		#define RX_VECT USART0_RX_vect
		#define TX_VECT USART0_UDRE_vect
		#define TXRX_REG UDR0
		#define TXRX_REG_EMPTY_FLAG UDRE0
		#define CONTROL_REG_A UCSR0A
		#define CONTROL_REG_B UCSR0B
		#define CONTROL_REG_C UCSR0C
		#define TX_INT_EN UDRIE0
		#define RX_INT_EN RXCIE0
		#define TX_EN TXEN0
		#define RX_EN RXEN0
		#define SEL_SIZE0 UCSZ00
		#define SEL_SIZE1 UCSZ01
		#define ASYNC_MODE U2X0
		#define BAUD_REGH UBRR0H
		#define BAUD_REGL UBRR0L		
	
	#elif (defined (__AVR_ATmega32A__) || defined (__AVR_ATmega16A__) || defined (__AVR_ATmega8A__))
		#define RX_VECT USART_RXC_vect
		#define TX_VECT USART_UDRE_vect
		#define TXRX_REG UDR
		#define TXRX_REG_EMPTY_FLAG UDRE
		#define CONTROL_REG_A UCSRA
		#define CONTROL_REG_B UCSRB
		#define CONTROL_REG_C UCSRC
		#define TX_INT_EN UDRIE
		#define RX_INT_EN RXCIE
		#define TX_EN TXEN
		#define RX_EN RXEN
		#define SEL_SIZE0 UCSZ0
		#define SEL_SIZE1 UCSZ1
		#define ASYNC_MODE U2X
		#define BAUD_REGH UBRRH
		#define BAUD_REGL UBRRL
		
	#else
		# warning "unknown CPU in UART.h"
		
	#endif
	
	#include <avr/io.h>					//needed for uint8_t 
	
	#define UART_RXerror_BufferFull	 1	//if RXbuffer is full when receiving a new byte, this bit is set in the ErrorByte
										
	//Change here for different baud rate 
	//#define BAUD 38400UL
	#define BAUD 9600UL
	#define MYUBRR (F_CPU/(8*BAUD))-1   //argument for UART_Init()

	uint8_t UART_sendByte(uint8_t inByte);
	void UART_Sendbyte_TEST( unsigned char data );
	void UART_Init( unsigned int ubrr);
	uint8_t UART_getByte(uint8_t *Byte);
	void UART_sendString(const char *s);
	uint8_t UART_sendNewLine(void);

#endif /* UART_H_ */