/*
 * UART.c
 *
 * Created: 13.07.2015 18:40:49
 * Author: Marc/Alex
 */ 

/******************************************************************************************************
Header Files
******************************************************************************************************/
#include <avr/io.h>				// needed for uint8_t and IO 
#include <avr/interrupt.h>		// interrupts 
#include "UART.h"				// header file with defines and prototypes 

/******************************************************************************************************
Defines and typedefs 
******************************************************************************************************/
#define UART_BUFFER_SIZE 64		// buffer Size for RX and TX buffer must be 2^n (8,16,32,64,128....)
#define UART_BUFFER_MASK (UART_BUFFER_SIZE-1)

typedef struct {
	uint8_t data[UART_BUFFER_SIZE];
	uint8_t read;
	uint8_t write;
	} UART_BUFFER;

/******************************************************************************************************
Global Variables 
******************************************************************************************************/	
static volatile UART_BUFFER UART_RXbuffer ={{}, 0,0};	// receive buffer 
static volatile UART_BUFFER UART_TXbuffer ={{}, 0,0};	// transmit buffer 
	
volatile uint8_t errorByte;								// volatile var to avoid errors of ISRs

/******************************************************************************************************
Interrupt service routine 
RX_vect gets executed when a new byte is received. This byte is put into the the receive buffer
******************************************************************************************************/
ISR(RX_VECT)//serial read interrupt
{
	uint8_t byteBuffer = TXRX_REG;
	uint8_t writeBuffer = UART_RXbuffer.write;
	uint8_t next = ((writeBuffer+1) & UART_BUFFER_MASK);
	
	if (UART_RXbuffer.read == next)
	{
		errorByte |= (1<<UART_RXerror_BufferFull);	//set error in Error Byte
	} 
	UART_RXbuffer.data[writeBuffer]=byteBuffer;		//write Byte into Buffer
	UART_RXbuffer.write=next;
	
	//ready with no error
}

/******************************************************************************************************
Interrupt service routine (serial transmit interrupt (Data Register empty Interrupt))
TX_vect gets executed when the send data register of the UART interface is empty. 
******************************************************************************************************/
// ISR (TX_VECT)	
// {
// 	uint8_t tempRead;
// 	
// 	if (!(UART_TXbuffer.read == UART_TXbuffer.write))	// if buffer not empty
// 	{
// 		tempRead = UART_TXbuffer.read;
// 		TXRX_REG = UART_TXbuffer.data[tempRead];		// get one byte and send it
// 		tempRead=  ( (tempRead+1) & UART_BUFFER_MASK);
// 		UART_TXbuffer.read = tempRead;
// 		
// 	}
// 	else //Buffer is Empty
// 	{
// 		CONTROL_REG_B &= ~(1<<TX_INT_EN);				//disable the UDRE (this) interrupt 
// 	}
// }

/******************************************************************************************************
Initializes the UART interface. Call this function before all other. Argument is defined in UART_BUF.h.
Example

UART_Init(MYUBRR);
UART_sendByte(myLocalByte);
...

Input: ubrr (value for UBRR0-Register), MYUBRR is defined in UART_BUF.h 
******************************************************************************************************/
void UART_Init( unsigned int ubrr)
{
	/*Set baud rate */
	BAUD_REGH = (unsigned char)(ubrr>>8);
	BAUD_REGL = (unsigned char)ubrr;

	//Enable receiver and transmitter and RX interrupt*/
	CONTROL_REG_B = (1<<RX_EN)/*|(1<<TX_EN)*/|(1<<RX_INT_EN);
	
	/* Set frame format: 8data, 1stop bit */	
	#if( defined (__AVR_ATmega328p__) || defined (__AVR_ATmega2560__))
		CONTROL_REG_C = (1<<SEL_SIZE0)|(1<<SEL_SIZE1);
	#elif (defined (__AVR_ATmega32A__) || defined (__AVR_ATmega16A__) || defined (__AVR_ATmega8A__))
		//For ATmega32A the URSEL bit has to be set before writing the control reg c is possible
		CONTROL_REG_C = (1<<URSEL)|(1<<SEL_SIZE0)|(1<<SEL_SIZE1);
	#else
		# warning "unknown CPU in UART_init()"
	#endif
	
	CONTROL_REG_A |= (1<<ASYNC_MODE); //double the speed with asynchronous UART mode
	//sei() necessary in main function for enabling interrupts
}

/******************************************************************************************************
 put one byte in the transmission buffer an enable transmission interrupt to send it
 
 Input:  Byte to be send
 Return: 0: Error (buffer is full)
		 1: ok
******************************************************************************************************/
uint8_t UART_sendByte(uint8_t inByte)
{
	cli();
	uint8_t next =((UART_TXbuffer.write+1) & UART_BUFFER_MASK);
	if (UART_TXbuffer.read == next)
		return 0;
	UART_TXbuffer.data[UART_TXbuffer.write]=inByte;
	UART_TXbuffer.write = next;
	CONTROL_REG_B |= (1<<TX_INT_EN);	//enable transmit interrupt (UART-Data-register-empty)
										//when the SEND Register is empty, it will be filled with one next Byte and send it
	sei();
	return 1;
}

/******************************************************************************************************
 Send one Byte without using buffer or interrupt. Just for testing 
 
 Input:  Byte to be send
******************************************************************************************************/
void UART_Sendbyte_TEST( unsigned char data )
{
	/* Wait for empty transmit buffer */
	while ( !( CONTROL_REG_A & (1<<TXRX_REG_EMPTY_FLAG)) )
	;
	/* Put data into buffer, sends the data */
	TXRX_REG = (char)(data);
}

/******************************************************************************************************
Send a String. UART_sendByte() get called in loop
 
Input:  String to be send
******************************************************************************************************/
void UART_sendString(const char *s)
{
	while (*s)
		UART_sendByte(*s++);
}

/******************************************************************************************************
 == UART_sendByte(0x0A) 
 
 Return: 0: Error (buffer is full)
		 1: ok
******************************************************************************************************/
uint8_t UART_sendNewLine(void)
{
	return UART_sendByte(0x0A);
}

/******************************************************************************************************
Get one Byte out of the receive Buffer
 
 Output: received Byte 
 Return: 0: Error (buffer is empty - no byte received)
		 1: ok
******************************************************************************************************/
uint8_t UART_getByte(uint8_t *Byte)
{
	if (UART_RXbuffer.read==UART_RXbuffer.write)	//if Buffer empty
		return 0;
	cli();
	*Byte = UART_RXbuffer.data[UART_RXbuffer.read];
	UART_RXbuffer.read = (UART_RXbuffer.read+1) & UART_BUFFER_MASK;
	sei();
	return 1;
}


