/*
 * spi.h
 *
 * Created: 18.07.2014 10:29:40
 *  Author: al1
 */ 

#ifndef SPI_H_
#define SPI_H_

#include <avr/io.h>

#if defined (__AVR_ATmega2560__) || (__AVR_ATmega8U2__) || (__AVR_ATmega16U2__) || (__AVR_ATmega32U2__)
	#define SPI_SS_PIN PORTB0
	#define SPI_SCK_PIN PORTB1
	#define SPI_MOSI_PIN PORTB2
	#define SPI_MISO_PIN PORTB3
#elif defined (__AVR_ATmega328P__) || (__AVR_ATmega8A__)
	#define SPI_SS_PIN PORTB2
	#define SPI_SCK_PIN PORTB5
	#define SPI_MOSI_PIN PORTB3
	#define SPI_MISO_PIN PORTB4
#else
	#error "unknown MC chek spi.h."
#endif	

uint8_t setupSpi(uint8_t prescaler, uint8_t interrupt);
uint8_t putByteSpi (uint8_t inByte);
uint8_t putByteSpiF (uint8_t inByte);

void resetSS (void);
void setSS (void);

uint8_t testMISO();

#endif