/*
 * spi.c
 *
 * Created: 18.07.2014 10:29:21
 *  Author: user
 */ 

/*
 * This Function initialize the SPI Module
 * 
 * Paramter:
 * IN:	prescaler		prescaler for SCK f_SCK = F_CPU/prescaler 
						prescaler must be element of {2,4,8,16,32,64,128}
   IN:	interrupt		if set SPI interrupt is enabled
 * Out: return value	0 if Error (wrong prescaler)
						1 else
*/


#include <avr/io.h>
#include "spi.h"

uint8_t setupSpi(uint8_t prescaler, uint8_t interrupt)
{
	//configure SPI Pins 
	DDRB |= (1<<SPI_MOSI_PIN); // output
    DDRB &= ~(1<<SPI_MISO_PIN); // input
    DDRB |= (1<<SPI_SCK_PIN);// output
    DDRB |= (1<<SPI_SS_PIN);// output
	PORTB |= (1<<SPI_SS_PIN); //SS = high
	
	if (!((prescaler==2)||(prescaler==4)||(prescaler==8)||(prescaler==16)||(prescaler==32)||(prescaler==64)||(prescaler==128)))
		return 0; //prescaler wrong
	
	if ((prescaler==2)||(prescaler==8)||(prescaler==32)||(prescaler==64))
		SPSR |= (1<<SPI2X);
	if ((prescaler==64)||(prescaler==128)||(prescaler==32)||(prescaler==46))
		SPCR |= (1<<SPR1);
	if ((prescaler==16)||(prescaler==128)||(prescaler==8)||(prescaler==64))
		SPCR |= (1<<SPR0);
	
	if (interrupt) //enable SPI interrupt
		SPCR |= (1<<SPIE);
	
	SPCR |= (1<<SPE)|(1<<MSTR)|(1<<CPOL)|(1<<CPHA); //clock invertet sample on 2nd/rising edge
	//SPCR |= (1<<DORD);
	return 1; //no Error

}

/*
 * This function shift in/out one Byte over the SPI Bus
 */

uint8_t putByteSpi (uint8_t inByte)
{
	SPDR = inByte; //put in Byte into the send-buffer
	while( !(SPSR & (1<<SPIF)) )
		;
	return SPDR;
}

void resetSS (void)
{
	PORTB  &= ~(1<<SPI_SS_PIN); //SS = low
}

void setSS (void)
{
	PORTB |= (1<<SPI_SS_PIN); //SS = high
}

uint8_t putByteSpiF (uint8_t inByte)
{
	resetSS();
	SPDR = inByte; //put in Byte into the send-buffer
	while( !(SPSR & (1<<SPIF)) )
		;
	setSS();
	return SPDR;
}

uint8_t testMISO()
{
	if (PINB & (1<<SPI_MISO_PIN))
		return 1;
	else
		return 0;
	
}