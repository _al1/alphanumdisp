/*
 * gpio.h
 *
 * Created: 31.01.2016 14:17:47
 *  Author: al1
 */ 


#ifndef GPIO_H_
#define GPIO_H_

	#include <avr/io.h>

	#define SHIFT_OE_PIN	PORTB1
	#define SHIFT_OE_PORT	PORTB
	
	#define  SHIFT_OUTPUT_ENABLE	(SHIFT_OE_PORT&=~(1<<SHIFT_OE_PIN))
	#define  SHIFT_OUTPUT_DISABlE	(SHIFT_OE_PORT|=(1<<SHIFT_OE_PIN))
	
	#define RESET_SS				PORTB  &= ~(1<<SPI_SS_PIN) //SS = low
	#define SET_SS					PORTB |= (1<<SPI_SS_PIN)   //SS = high

#endif /* GPIO_H_ */